package utils

import "regexp"

func standardString(branchName string) string {
	re := regexp.MustCompile(`^origin`)
	branchName = re.ReplaceAllString(branchName, "")
	reAll := regexp.MustCompile(`[^\w]`)
	branchName = reAll.ReplaceAllString(branchName, "-")
	return branchName

}

